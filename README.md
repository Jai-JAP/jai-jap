<h1 align="center">Hi <img src="https://emojipedia-us.s3.amazonaws.com/source/microsoft-teams/337/waving-hand_1f44b.png" width="40">, I'm Jai</h1>
<h3 align="center">Passionate learner and developer from 🇮🇳 India 🇮🇳<h1>
<p align="center">
<img src="https://img.shields.io/github/followers/Jai-JAP.svg?style=social&label=Follow%20Me" width="125">
<p>

## I am ...
- the one who massively improved the [PiKISS GUI](https://github.com/Jai-JAP/pikiss-gui) (by adding automated syncinging of apps with [piKiss](https://github.com/jmcerrejon/piKiss)) which was originally created by [Krishenrikesn](https://github.com/krishenriksen/pikiss-gui)
- the creator of [TwisteRPi Imager](https://github.com/Jai-JAP/TwisteRPi-Imager), RPi-Imager with Twister OS inside alongside all other OSes. ***[DEPRECATED]***
- the one who ported [Tabby](https://github.com/Eugeny/Tabby) to Linux armhf, arm64 and Windows arm64 platforms.
  - <details>
      https://github.com/Eugeny/tabby/pull/6612
      https://github.com/Eugeny/tabby/pull/5907
    </details>
- the one who ported [FreeTube](https://github.com/FreeTubeApp/FreeTube) to Linux armhf and Windows arm64 platforms.
  - <details>
      https://github.com/FreeTubeApp/FreeTube/pull/2113
    </details>
- also the one hosting packages of some apps compiled for arm Linux. 
  - [re3 GTA](https://github.com/Jai-JAP/RPi-GTA-re)
  - [Hyper](https://github.com/Jai-JAP/hyper-arm-builds) - (New releases are automatically compiled for linux armv7l & arm64 using GitHub Workflows.)
  - [Tabby](https://github.com/Jai-JAP/tabby-arm-builds) - (New releases are automatically compiled for linux armv7l & arm64 using GitHub Workflows.) ***[DEPRECATED]*** See [here](https://github.com/Eugeny/tabby/pull/6612)
  - [FreeTube](https://github.com/Jai-JAP/freetube-armhf-builds) (armhf only as arm64 packages are available on official [repo](https://github.com/FreeTubeApp/FreeTube)) ***[DEPRECATED]*** See [here](https://github.com/FreeTubeApp/FreeTube/pull/2113)
- I also have a unofficial full archived mirror of re3 i.e; Reverse Engineered GTA III and Vice City (Including wiki) [here](https://github.com/Jai-JAP/re-GTA)
- [Here](https://github.com/Jai-JAP/starred-repos) is the list of my starred repos
  
---
  
- <img src="https://emojipedia-us.s3.amazonaws.com/source/microsoft-teams/337/telescope_1f52d.png" width="22"> I’m currently working on Pi-apps and [more...](https://github.com/Jai-JAP?tab=repositories)
- <img src="https://emojipedia-us.s3.amazonaws.com/source/microsoft-teams/337/seedling_1f331.png" width="22"> I’m currently learning Python <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Python-logo-notext.svg/110px-Python-logo-notext.svg.png" height="22">, C <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/C_Programming_Language.svg/380px-C_Programming_Language.svg.png" height="22"> and improving my BASH <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Bash_Logo_Colored.svg/240px-Bash_Logo_Colored.svg.png" height="22"> skills
- <img src="https://emojipedia-us.s3.amazonaws.com/source/microsoft-teams/337/people-with-bunny-ears_1f46f.png" width="22"> I’m looking to collaborate on [Botspot/pi-apps](https://github.com/Botspot/pi-apps)
- <img src="https://emojipedia-us.s3.amazonaws.com/source/microsoft-teams/337/speech-balloon_1f4ac.png" width="22"> Ask me about Linux, Bash and RaspberryPi
- <img src="https://emojipedia-us.s3.amazonaws.com/source/microsoft-teams/337/closed-mailbox-with-raised-flag_1f4eb.png" width="22"> How to reach me: 
  - Discord ***[@Jai-JAP#7152](https://discord.com/users/812585254303825930)***
  - Email ***[jai.jap.318@gmail.com](mailto:jai.jap.318@gmail.com)***
- <img src="https://emojipedia-us.s3.amazonaws.com/source/microsoft-teams/337/high-voltage_26a1.png" width="22"> Motto: Use Linux to ditch Windows <img src="https://emojipedia-us.s3.amazonaws.com/source/microsoft-teams/337/winking-face-with-tongue_1f61c.png" width="20">

## My GitHub Stats

<p align="center">
<img src="https://github.com/jai-jap/gh-stats/blob/master/generated/overview.svg" alt="GitHub Stats">
<img src="https://github.com/jai-jap/gh-stats/blob/master/generated/languages.svg" alt="Most used languages"><br/><br/>
<img src="https://github-readme-streak-stats.herokuapp.com/?user=Jai-JAP" alt="Streak Status"><br/><br/>
<img src="https://github-profile-trophy.vercel.app/?username=Jai-JAP" alt="Trophies"><br/><br/>
<img src="https://raw.githubusercontent.com/Jai-JAP/Jai-JAP/master/achievements.svg" alt="Achievements" width="600"><br/><br/>
<img src="https://raw.githubusercontent.com/Jai-JAP/Jai-JAP/master/overview.svg" alt="Overview" width="600"><br/><br/>
<img src="https://github-readme-stats.vercel.app/api?username=Jai-JAP&show_icons=true" alt="Ranking">
<p/>

